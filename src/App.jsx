import './App.css';
import TodoList from './TodoList';

const isActive = true;
const output = isActive ? 'Active' : 'Inactive';

function App() {
  return (
    <div>
      <TodoList />
      <div>{output}</div>
    </div>
  );
}

export default App;
