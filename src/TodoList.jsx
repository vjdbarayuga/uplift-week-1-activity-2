// eslint-disable-next-line no-unused-vars
import React from 'react';

const TodoList = () => {
  return (
    <div>
      <h1>Todo List</h1>
      <ul>
        <li>Task 1</li>
        <li>Task 2</li>
        <li>Task 3</li>
      </ul>
    </div>
  );
};

export default TodoList;
